EESchema Schematic File Version 2
LIBS:BinaryClock-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:demux
LIBS:simon
LIBS:BinaryClock-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "Binary clock"
Date ""
Rev "1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 700  700  1400 1000
U 5785D1A6
F0 "Power and MCU" 60
F1 "power_mcu.sch" 60
$EndSheet
$Sheet
S 2300 700  1400 1000
U 57854CC7
F0 "led_matrix" 60
F1 "led_matrix.sch" 60
$EndSheet
$Comp
L GND #PWR01
U 1 1 57A9F461
P 8300 1700
F 0 "#PWR01" H 8300 1450 50  0001 C CNN
F 1 "GND" H 8300 1550 50  0000 C CNN
F 2 "" H 8300 1700 50  0000 C CNN
F 3 "" H 8300 1700 50  0000 C CNN
	1    8300 1700
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 57A9F475
P 8300 1150
F 0 "#FLG02" H 8300 1245 50  0001 C CNN
F 1 "PWR_FLAG" H 8300 1330 50  0000 C CNN
F 2 "" H 8300 1150 50  0000 C CNN
F 3 "" H 8300 1150 50  0000 C CNN
	1    8300 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 1700 8300 1150
$Comp
L GNDA #PWR03
U 1 1 57C04D03
P 8950 1750
F 0 "#PWR03" H 8950 1500 50  0001 C CNN
F 1 "GNDA" H 8950 1600 50  0000 C CNN
F 2 "" H 8950 1750 50  0000 C CNN
F 3 "" H 8950 1750 50  0000 C CNN
	1    8950 1750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 57C04D17
P 9300 1750
F 0 "#PWR04" H 9300 1500 50  0001 C CNN
F 1 "GND" H 9300 1600 50  0000 C CNN
F 2 "" H 9300 1750 50  0000 C CNN
F 3 "" H 9300 1750 50  0000 C CNN
	1    9300 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 1750 8950 1600
Wire Wire Line
	8950 1600 9300 1600
Wire Wire Line
	9300 1600 9300 1750
$EndSCHEMATC
