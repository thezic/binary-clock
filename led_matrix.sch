EESchema Schematic File Version 2
LIBS:BinaryClock-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:demux
LIBS:simon
LIBS:BinaryClock-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Binary clock"
Date ""
Rev "1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR038
U 1 1 5785BB00
P 2450 2550
F 0 "#PWR038" H 2450 2300 50  0001 C CNN
F 1 "GND" H 2450 2400 50  0000 C CNN
F 2 "" H 2450 2550 50  0000 C CNN
F 3 "" H 2450 2550 50  0000 C CNN
	1    2450 2550
	1    0    0    -1  
$EndComp
NoConn ~ 3200 1850
NoConn ~ 3200 1750
$Comp
L CD74HC237 U3
U 1 1 5785BB08
P 2650 1550
F 0 "U3" H 2800 800 60  0000 C CNN
F 1 "CD74HC137" H 2500 1450 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-16_4.4x5mm_Pitch0.65mm" H 2650 1600 60  0001 C CNN
F 3 "" H 2650 1600 60  0000 C CNN
	1    2650 1550
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 5785BB2A
P 8900 3700
F 0 "R19" V 8980 3700 50  0000 C CNN
F 1 "390" V 8900 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8830 3700 50  0001 C CNN
F 3 "" H 8900 3700 50  0000 C CNN
	1    8900 3700
	0    1    1    0   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D4
U 1 1 5785BB54
P 3750 3400
AR Path="/5785BB54" Ref="D4"  Part="1" 
AR Path="/57854CC7/5785BB54" Ref="D4"  Part="1" 
F 0 "D4" H 3750 3500 50  0000 C CNN
F 1 "APT1608LSECK" H 3750 3250 50  0000 C CNN
F 2 "LEDs:LED_0603" H 3750 3400 50  0001 C CNN
F 3 "" H 3750 3400 50  0000 C CNN
	1    3750 3400
	-1   0    0    1   
$EndComp
Text GLabel 9250 3700 2    60   Input ~ 0
LED_ROW0
Text GLabel 9250 4300 2    60   Input ~ 0
LED_ROW1
Text GLabel 9250 4900 2    60   Input ~ 0
LED_ROW2
Text GLabel 9250 5500 2    60   Input ~ 0
LED_ROW3
Text GLabel 1350 2050 0    60   Input ~ 0
LED_PWM
$Comp
L Q_PMOS_GSD Q3
U 1 1 57908C3A
P 3550 2650
F 0 "Q3" V 3900 2550 50  0000 R CNN
F 1 "BSS215P" V 3800 2800 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 3750 2750 50  0001 C CNN
F 3 "" H 3550 2650 50  0000 C CNN
	1    3550 2650
	-1   0    0    1   
$EndComp
Text GLabel 1350 1250 0    60   Input ~ 0
LED_COL_ADDR0
Text GLabel 1350 1350 0    60   Input ~ 0
LED_COL_ADDR1
Text GLabel 1350 1450 0    60   Input ~ 0
LED_COL_ADDR2
Text Notes 8450 2550 0    60   ~ 0
Ciss = 260pF\nR = 4.7k\nfc = 130kHz
$Comp
L R R17
U 1 1 5790E27A
P 7350 1950
F 0 "R17" H 7450 1950 50  0000 C CNN
F 1 "4k7" V 7350 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 7280 1950 50  0001 C CNN
F 3 "" H 7350 1950 50  0000 C CNN
	1    7350 1950
	1    0    0    -1  
$EndComp
$Comp
L Q_PMOS_GSD Q6
U 1 1 5790F87F
P 6250 2600
F 0 "Q6" V 6600 2500 50  0000 R CNN
F 1 "BSS215P" V 6500 2750 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 6450 2700 50  0001 C CNN
F 3 "" H 6250 2600 50  0000 C CNN
	1    6250 2600
	-1   0    0    1   
$EndComp
$Comp
L Q_PMOS_GSD Q7
U 1 1 5790F93E
P 7150 2600
F 0 "Q7" V 7500 2500 50  0000 R CNN
F 1 "BSS215P" V 7400 2750 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 7350 2700 50  0001 C CNN
F 3 "" H 7150 2600 50  0000 C CNN
	1    7150 2600
	-1   0    0    1   
$EndComp
$Comp
L Q_PMOS_GSD Q8
U 1 1 5790F9FC
P 8050 2600
F 0 "Q8" V 8400 2500 50  0000 R CNN
F 1 "BSS215P" V 8300 2700 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 8250 2700 50  0001 C CNN
F 3 "" H 8050 2600 50  0000 C CNN
	1    8050 2600
	-1   0    0    1   
$EndComp
Text Notes 8500 3600 0    60   ~ 0
Vf =1.8V\nIled = 2mA
$Comp
L R R20
U 1 1 5791094F
P 8900 4300
F 0 "R20" V 8980 4300 50  0000 C CNN
F 1 "390" V 8900 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8830 4300 50  0001 C CNN
F 3 "" H 8900 4300 50  0000 C CNN
	1    8900 4300
	0    1    1    0   
$EndComp
$Comp
L R R21
U 1 1 57910A09
P 8900 4900
F 0 "R21" V 8980 4900 50  0000 C CNN
F 1 "390" V 8900 4900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8830 4900 50  0001 C CNN
F 3 "" H 8900 4900 50  0000 C CNN
	1    8900 4900
	0    1    1    0   
$EndComp
$Comp
L R R22
U 1 1 57910AC9
P 8900 5500
F 0 "R22" V 8980 5500 50  0000 C CNN
F 1 "390" V 8900 5500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8830 5500 50  0001 C CNN
F 3 "" H 8900 5500 50  0000 C CNN
	1    8900 5500
	0    1    1    0   
$EndComp
$Comp
L C_Small C18
U 1 1 57A9ACFF
P 2900 750
F 0 "C18" V 2950 800 50  0000 L CNN
F 1 "100n" V 2950 500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2900 750 50  0001 C CNN
F 3 "" H 2900 750 50  0000 C CNN
	1    2900 750 
	0    1    1    0   
$EndComp
$Comp
L GND #PWR039
U 1 1 57A9B8E9
P 3250 850
F 0 "#PWR039" H 3250 600 50  0001 C CNN
F 1 "GND" H 3250 700 50  0000 C CNN
F 2 "" H 3250 850 50  0000 C CNN
F 3 "" H 3250 850 50  0000 C CNN
	1    3250 850 
	1    0    0    -1  
$EndComp
$Comp
L Q_PMOS_GSD Q5
U 1 1 5790F7CC
P 5350 2600
F 0 "Q5" V 5700 2500 50  0000 R CNN
F 1 "BSS215P" V 5600 2750 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5550 2700 50  0001 C CNN
F 3 "" H 5350 2600 50  0000 C CNN
	1    5350 2600
	-1   0    0    1   
$EndComp
$Comp
L Q_PMOS_GSD Q4
U 1 1 5790F710
P 4450 2600
F 0 "Q4" V 4800 2500 50  0000 R CNN
F 1 "BSS215P" V 4700 2750 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 4650 2700 50  0001 C CNN
F 3 "" H 4450 2600 50  0000 C CNN
	1    4450 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	9250 5500 9050 5500
Wire Wire Line
	9250 4900 9050 4900
Wire Wire Line
	9250 4300 9050 4300
Wire Wire Line
	9050 3700 9250 3700
Wire Wire Line
	2450 700  2450 850 
Wire Wire Line
	2450 2450 2450 2550
Wire Wire Line
	3200 1650 8250 1700
Wire Wire Line
	3200 1550 7350 1550
Wire Wire Line
	3200 1450 6450 1450
Wire Wire Line
	3200 1350 5550 1350
Wire Wire Line
	3200 1250 4650 1250
Wire Wire Line
	3200 1150 3750 1150
Connection ~ 8450 5500
Wire Wire Line
	8450 5500 8450 5200
Connection ~ 7550 5500
Wire Wire Line
	7550 5500 7550 5200
Connection ~ 6650 5500
Wire Wire Line
	6650 5500 6650 5200
Connection ~ 5750 5500
Wire Wire Line
	5750 5500 5750 5200
Connection ~ 4850 5500
Wire Wire Line
	4850 5200 4850 5500
Connection ~ 4350 4600
Wire Wire Line
	4350 5200 4450 5200
Wire Wire Line
	3950 5500 8750 5500
Wire Wire Line
	3950 5200 3950 5500
Connection ~ 7950 4600
Wire Wire Line
	7950 5200 8050 5200
Connection ~ 7050 4600
Wire Wire Line
	7050 5200 7150 5200
Connection ~ 6150 4600
Wire Wire Line
	6150 5200 6250 5200
Connection ~ 5250 4600
Wire Wire Line
	5250 5200 5350 5200
Connection ~ 3450 4600
Wire Wire Line
	3450 5200 3550 5200
Connection ~ 8450 4900
Wire Wire Line
	8450 4900 8450 4600
Connection ~ 7550 4900
Wire Wire Line
	7550 4900 7550 4600
Connection ~ 6650 4900
Wire Wire Line
	6650 4900 6650 4600
Connection ~ 5750 4900
Wire Wire Line
	5750 4900 5750 4600
Connection ~ 4850 4900
Wire Wire Line
	4850 4600 4850 4900
Wire Wire Line
	3950 4900 8750 4900
Wire Wire Line
	3950 4600 3950 4900
Connection ~ 7950 4000
Wire Wire Line
	7950 4600 8050 4600
Connection ~ 7050 4000
Wire Wire Line
	7050 4600 7150 4600
Connection ~ 6150 4000
Wire Wire Line
	6150 4600 6250 4600
Connection ~ 5250 4000
Wire Wire Line
	5250 4600 5350 4600
Connection ~ 4350 4000
Wire Wire Line
	4350 4600 4450 4600
Connection ~ 3450 4000
Wire Wire Line
	3450 4600 3550 4600
Connection ~ 8450 4300
Wire Wire Line
	8450 4300 8450 4000
Connection ~ 7550 4300
Wire Wire Line
	7550 4300 7550 4000
Connection ~ 6650 4300
Wire Wire Line
	6650 4300 6650 4000
Connection ~ 5750 4300
Wire Wire Line
	5750 4300 5750 4000
Connection ~ 4850 4300
Wire Wire Line
	4850 4000 4850 4300
Wire Wire Line
	3950 4300 8750 4300
Wire Wire Line
	3950 4000 3950 4300
Connection ~ 7950 3400
Wire Wire Line
	7950 4000 8050 4000
Connection ~ 7050 3400
Wire Wire Line
	7050 4000 7150 4000
Connection ~ 6150 3400
Wire Wire Line
	6150 4000 6250 4000
Connection ~ 5250 3400
Wire Wire Line
	5250 4000 5350 4000
Connection ~ 4350 3400
Wire Wire Line
	4350 4000 4450 4000
Connection ~ 3450 3400
Wire Wire Line
	3450 4000 3550 4000
Connection ~ 8450 3700
Wire Wire Line
	8450 3700 8450 3400
Connection ~ 7550 3700
Wire Wire Line
	7550 3700 7550 3400
Connection ~ 6650 3700
Wire Wire Line
	6650 3700 6650 3400
Connection ~ 5750 3700
Wire Wire Line
	5750 3700 5750 3400
Connection ~ 4850 3700
Wire Wire Line
	4850 3400 4850 3700
Wire Wire Line
	3950 3700 8750 3700
Wire Wire Line
	3950 3400 3950 3700
Wire Wire Line
	7950 3400 8050 3400
Wire Wire Line
	7950 2800 7950 5200
Wire Wire Line
	7050 3400 7150 3400
Wire Wire Line
	7050 2800 7050 5200
Wire Wire Line
	6150 3400 6250 3400
Wire Wire Line
	5250 2800 5250 5200
Wire Wire Line
	5350 3400 5250 3400
Wire Wire Line
	4350 2800 4350 5200
Wire Wire Line
	4450 3400 4350 3400
Wire Wire Line
	3450 2850 3450 5200
Wire Wire Line
	3550 3400 3450 3400
Wire Wire Line
	1350 1250 1850 1250
Wire Wire Line
	1350 1350 1850 1350
Wire Wire Line
	1350 1450 1850 1450
Wire Wire Line
	3750 1150 3750 1800
Wire Wire Line
	6150 2800 6150 5200
Wire Wire Line
	8250 1700 8250 1800
Wire Wire Line
	7350 1550 7350 1800
Wire Wire Line
	6450 1450 6450 1800
Wire Wire Line
	5550 1350 5550 1800
Wire Wire Line
	4650 1250 4650 1800
Wire Wire Line
	2450 750  2800 750 
Connection ~ 2450 750 
Wire Wire Line
	3250 850  3250 750 
Wire Wire Line
	3250 750  3000 750 
Wire Wire Line
	1700 2500 2450 2500
Wire Wire Line
	1700 1750 1700 2500
Wire Wire Line
	1700 1750 1850 1750
Connection ~ 2450 2500
Wire Wire Line
	8250 2100 8250 2600
Wire Wire Line
	6450 2600 6450 2100
Wire Wire Line
	5550 2100 5550 2600
Wire Wire Line
	4650 2600 4650 2100
Wire Wire Line
	3750 2100 3750 2650
Wire Wire Line
	7350 2100 7350 2600
Wire Wire Line
	7950 2400 7950 2300
Wire Wire Line
	3450 2450 3450 2300
Wire Wire Line
	4350 2400 4350 2300
Wire Wire Line
	5250 2400 5250 2300
Wire Wire Line
	6150 2400 6150 2300
Wire Wire Line
	7050 2400 7050 2300
$Comp
L LED-RESCUE-BinaryClock D8
U 1 1 57FF807D
P 4650 3400
AR Path="/57FF807D" Ref="D8"  Part="1" 
AR Path="/57854CC7/57FF807D" Ref="D8"  Part="1" 
F 0 "D8" H 4650 3500 50  0000 C CNN
F 1 "APT1608LSECK" H 4650 3250 50  0000 C CNN
F 2 "LEDs:LED_0603" H 4650 3400 50  0001 C CNN
F 3 "" H 4650 3400 50  0000 C CNN
	1    4650 3400
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D12
U 1 1 57FF80F0
P 5550 3400
AR Path="/57FF80F0" Ref="D12"  Part="1" 
AR Path="/57854CC7/57FF80F0" Ref="D12"  Part="1" 
F 0 "D12" H 5550 3500 50  0000 C CNN
F 1 "APT1608LSECK" H 5550 3250 50  0000 C CNN
F 2 "LEDs:LED_0603" H 5550 3400 50  0001 C CNN
F 3 "" H 5550 3400 50  0000 C CNN
	1    5550 3400
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D16
U 1 1 57FF815C
P 6450 3400
AR Path="/57FF815C" Ref="D16"  Part="1" 
AR Path="/57854CC7/57FF815C" Ref="D16"  Part="1" 
F 0 "D16" H 6450 3500 50  0000 C CNN
F 1 "APT1608LSECK" H 6450 3250 50  0000 C CNN
F 2 "LEDs:LED_0603" H 6450 3400 50  0001 C CNN
F 3 "" H 6450 3400 50  0000 C CNN
	1    6450 3400
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D20
U 1 1 57FF81C7
P 7350 3400
AR Path="/57FF81C7" Ref="D20"  Part="1" 
AR Path="/57854CC7/57FF81C7" Ref="D20"  Part="1" 
F 0 "D20" H 7350 3500 50  0000 C CNN
F 1 "APT1608LSECK" H 7350 3250 50  0000 C CNN
F 2 "LEDs:LED_0603" H 7350 3400 50  0001 C CNN
F 3 "" H 7350 3400 50  0000 C CNN
	1    7350 3400
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D24
U 1 1 57FF823A
P 8250 3400
AR Path="/57FF823A" Ref="D24"  Part="1" 
AR Path="/57854CC7/57FF823A" Ref="D24"  Part="1" 
F 0 "D24" H 8250 3500 50  0000 C CNN
F 1 "APT1608LSECK" H 8250 3250 50  0000 C CNN
F 2 "LEDs:LED_0603" H 8250 3400 50  0001 C CNN
F 3 "" H 8250 3400 50  0000 C CNN
	1    8250 3400
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D5
U 1 1 57FF82AC
P 3750 4000
AR Path="/57FF82AC" Ref="D5"  Part="1" 
AR Path="/57854CC7/57FF82AC" Ref="D5"  Part="1" 
F 0 "D5" H 3750 4100 50  0000 C CNN
F 1 "APT1608LSECK" H 3750 3850 50  0000 C CNN
F 2 "LEDs:LED_0603" H 3750 4000 50  0001 C CNN
F 3 "" H 3750 4000 50  0000 C CNN
	1    3750 4000
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D6
U 1 1 57FF8321
P 3750 4600
AR Path="/57FF8321" Ref="D6"  Part="1" 
AR Path="/57854CC7/57FF8321" Ref="D6"  Part="1" 
F 0 "D6" H 3750 4700 50  0000 C CNN
F 1 "APT1608LSECK" H 3750 4450 50  0000 C CNN
F 2 "LEDs:LED_0603" H 3750 4600 50  0001 C CNN
F 3 "" H 3750 4600 50  0000 C CNN
	1    3750 4600
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D7
U 1 1 57FF8396
P 3750 5200
AR Path="/57FF8396" Ref="D7"  Part="1" 
AR Path="/57854CC7/57FF8396" Ref="D7"  Part="1" 
F 0 "D7" H 3750 5300 50  0000 C CNN
F 1 "APT1608LSECK" H 3750 5050 50  0000 C CNN
F 2 "LEDs:LED_0603" H 3750 5200 50  0001 C CNN
F 3 "" H 3750 5200 50  0000 C CNN
	1    3750 5200
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D9
U 1 1 57FF845E
P 4650 4000
AR Path="/57FF845E" Ref="D9"  Part="1" 
AR Path="/57854CC7/57FF845E" Ref="D9"  Part="1" 
F 0 "D9" H 4650 4100 50  0000 C CNN
F 1 "APT1608LSECK" H 4650 3850 50  0000 C CNN
F 2 "LEDs:LED_0603" H 4650 4000 50  0001 C CNN
F 3 "" H 4650 4000 50  0000 C CNN
	1    4650 4000
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D10
U 1 1 57FF8464
P 4650 4600
AR Path="/57FF8464" Ref="D10"  Part="1" 
AR Path="/57854CC7/57FF8464" Ref="D10"  Part="1" 
F 0 "D10" H 4650 4700 50  0000 C CNN
F 1 "APT1608LSECK" H 4650 4450 50  0000 C CNN
F 2 "LEDs:LED_0603" H 4650 4600 50  0001 C CNN
F 3 "" H 4650 4600 50  0000 C CNN
	1    4650 4600
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D11
U 1 1 57FF846A
P 4650 5200
AR Path="/57FF846A" Ref="D11"  Part="1" 
AR Path="/57854CC7/57FF846A" Ref="D11"  Part="1" 
F 0 "D11" H 4650 5300 50  0000 C CNN
F 1 "APT1608LSECK" H 4650 5050 50  0000 C CNN
F 2 "LEDs:LED_0603" H 4650 5200 50  0001 C CNN
F 3 "" H 4650 5200 50  0000 C CNN
	1    4650 5200
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D13
U 1 1 57FF8510
P 5550 4000
AR Path="/57FF8510" Ref="D13"  Part="1" 
AR Path="/57854CC7/57FF8510" Ref="D13"  Part="1" 
F 0 "D13" H 5550 4100 50  0000 C CNN
F 1 "APT1608LSECK" H 5550 3850 50  0000 C CNN
F 2 "LEDs:LED_0603" H 5550 4000 50  0001 C CNN
F 3 "" H 5550 4000 50  0000 C CNN
	1    5550 4000
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D14
U 1 1 57FF8516
P 5550 4600
AR Path="/57FF8516" Ref="D14"  Part="1" 
AR Path="/57854CC7/57FF8516" Ref="D14"  Part="1" 
F 0 "D14" H 5550 4700 50  0000 C CNN
F 1 "APT1608LSECK" H 5550 4450 50  0000 C CNN
F 2 "LEDs:LED_0603" H 5550 4600 50  0001 C CNN
F 3 "" H 5550 4600 50  0000 C CNN
	1    5550 4600
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D15
U 1 1 57FF851C
P 5550 5200
AR Path="/57FF851C" Ref="D15"  Part="1" 
AR Path="/57854CC7/57FF851C" Ref="D15"  Part="1" 
F 0 "D15" H 5550 5300 50  0000 C CNN
F 1 "APT1608LSECK" H 5550 5050 50  0000 C CNN
F 2 "LEDs:LED_0603" H 5550 5200 50  0001 C CNN
F 3 "" H 5550 5200 50  0000 C CNN
	1    5550 5200
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D17
U 1 1 57FF85E0
P 6450 4000
AR Path="/57FF85E0" Ref="D17"  Part="1" 
AR Path="/57854CC7/57FF85E0" Ref="D17"  Part="1" 
F 0 "D17" H 6450 4100 50  0000 C CNN
F 1 "APT1608LSECK" H 6450 3850 50  0000 C CNN
F 2 "LEDs:LED_0603" H 6450 4000 50  0001 C CNN
F 3 "" H 6450 4000 50  0000 C CNN
	1    6450 4000
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D18
U 1 1 57FF85E6
P 6450 4600
AR Path="/57FF85E6" Ref="D18"  Part="1" 
AR Path="/57854CC7/57FF85E6" Ref="D18"  Part="1" 
F 0 "D18" H 6450 4700 50  0000 C CNN
F 1 "APT1608LSECK" H 6450 4450 50  0000 C CNN
F 2 "LEDs:LED_0603" H 6450 4600 50  0001 C CNN
F 3 "" H 6450 4600 50  0000 C CNN
	1    6450 4600
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D19
U 1 1 57FF85EC
P 6450 5200
AR Path="/57FF85EC" Ref="D19"  Part="1" 
AR Path="/57854CC7/57FF85EC" Ref="D19"  Part="1" 
F 0 "D19" H 6450 5300 50  0000 C CNN
F 1 "APT1608LSECK" H 6450 5050 50  0000 C CNN
F 2 "LEDs:LED_0603" H 6450 5200 50  0001 C CNN
F 3 "" H 6450 5200 50  0000 C CNN
	1    6450 5200
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D21
U 1 1 57FF86D7
P 7350 4000
AR Path="/57FF86D7" Ref="D21"  Part="1" 
AR Path="/57854CC7/57FF86D7" Ref="D21"  Part="1" 
F 0 "D21" H 7350 4100 50  0000 C CNN
F 1 "APT1608LSECK" H 7350 3850 50  0000 C CNN
F 2 "LEDs:LED_0603" H 7350 4000 50  0001 C CNN
F 3 "" H 7350 4000 50  0000 C CNN
	1    7350 4000
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D22
U 1 1 57FF86DD
P 7350 4600
AR Path="/57FF86DD" Ref="D22"  Part="1" 
AR Path="/57854CC7/57FF86DD" Ref="D22"  Part="1" 
F 0 "D22" H 7350 4700 50  0000 C CNN
F 1 "APT1608LSECK" H 7350 4450 50  0000 C CNN
F 2 "LEDs:LED_0603" H 7350 4600 50  0001 C CNN
F 3 "" H 7350 4600 50  0000 C CNN
	1    7350 4600
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D23
U 1 1 57FF86E3
P 7350 5200
AR Path="/57FF86E3" Ref="D23"  Part="1" 
AR Path="/57854CC7/57FF86E3" Ref="D23"  Part="1" 
F 0 "D23" H 7350 5300 50  0000 C CNN
F 1 "APT1608LSECK" H 7350 5050 50  0000 C CNN
F 2 "LEDs:LED_0603" H 7350 5200 50  0001 C CNN
F 3 "" H 7350 5200 50  0000 C CNN
	1    7350 5200
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D25
U 1 1 57FF87B3
P 8250 4000
AR Path="/57FF87B3" Ref="D25"  Part="1" 
AR Path="/57854CC7/57FF87B3" Ref="D25"  Part="1" 
F 0 "D25" H 8250 4100 50  0000 C CNN
F 1 "APT1608LSECK" H 8250 3850 50  0000 C CNN
F 2 "LEDs:LED_0603" H 8250 4000 50  0001 C CNN
F 3 "" H 8250 4000 50  0000 C CNN
	1    8250 4000
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D26
U 1 1 57FF87B9
P 8250 4600
AR Path="/57FF87B9" Ref="D26"  Part="1" 
AR Path="/57854CC7/57FF87B9" Ref="D26"  Part="1" 
F 0 "D26" H 8250 4700 50  0000 C CNN
F 1 "APT1608LSECK" H 8250 4450 50  0000 C CNN
F 2 "LEDs:LED_0603" H 8250 4600 50  0001 C CNN
F 3 "" H 8250 4600 50  0000 C CNN
	1    8250 4600
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-BinaryClock D27
U 1 1 57FF87BF
P 8250 5200
AR Path="/57FF87BF" Ref="D27"  Part="1" 
AR Path="/57854CC7/57FF87BF" Ref="D27"  Part="1" 
F 0 "D27" H 8250 5300 50  0000 C CNN
F 1 "APT1608LSECK" H 8250 5050 50  0000 C CNN
F 2 "LEDs:LED_0603" H 8250 5200 50  0001 C CNN
F 3 "" H 8250 5200 50  0000 C CNN
	1    8250 5200
	-1   0    0    1   
$EndComp
$Comp
L R R18
U 1 1 57FF904E
P 8250 1950
F 0 "R18" H 8350 1950 50  0000 C CNN
F 1 "4k7" V 8250 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 8180 1950 50  0001 C CNN
F 3 "" H 8250 1950 50  0000 C CNN
	1    8250 1950
	1    0    0    -1  
$EndComp
$Comp
L R R16
U 1 1 57FF90EA
P 6450 1950
F 0 "R16" H 6550 1950 50  0000 C CNN
F 1 "4k7" V 6450 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 6380 1950 50  0001 C CNN
F 3 "" H 6450 1950 50  0000 C CNN
	1    6450 1950
	1    0    0    -1  
$EndComp
$Comp
L R R15
U 1 1 57FF919C
P 5550 1950
F 0 "R15" H 5650 1950 50  0000 C CNN
F 1 "4k7" V 5550 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5480 1950 50  0001 C CNN
F 3 "" H 5550 1950 50  0000 C CNN
	1    5550 1950
	1    0    0    -1  
$EndComp
$Comp
L R R14
U 1 1 57FF923F
P 4650 1950
F 0 "R14" H 4750 1950 50  0000 C CNN
F 1 "4k7" V 4650 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4580 1950 50  0001 C CNN
F 3 "" H 4650 1950 50  0000 C CNN
	1    4650 1950
	1    0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 57FF92EA
P 3750 1950
F 0 "R13" H 3850 1950 50  0000 C CNN
F 1 "4k7" V 3750 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3680 1950 50  0001 C CNN
F 3 "" H 3750 1950 50  0000 C CNN
	1    3750 1950
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR040
U 1 1 580779A4
P 2450 700
F 0 "#PWR040" H 2450 550 50  0001 C CNN
F 1 "VCC" H 2450 850 50  0000 C CNN
F 2 "" H 2450 700 50  0000 C CNN
F 3 "" H 2450 700 50  0000 C CNN
	1    2450 700 
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR041
U 1 1 58077E8F
P 3450 2300
F 0 "#PWR041" H 3450 2150 50  0001 C CNN
F 1 "VCC" H 3450 2450 50  0000 C CNN
F 2 "" H 3450 2300 50  0000 C CNN
F 3 "" H 3450 2300 50  0000 C CNN
	1    3450 2300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR042
U 1 1 58077F33
P 4350 2300
F 0 "#PWR042" H 4350 2150 50  0001 C CNN
F 1 "VCC" H 4350 2450 50  0000 C CNN
F 2 "" H 4350 2300 50  0000 C CNN
F 3 "" H 4350 2300 50  0000 C CNN
	1    4350 2300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR043
U 1 1 58077FD7
P 5250 2300
F 0 "#PWR043" H 5250 2150 50  0001 C CNN
F 1 "VCC" H 5250 2450 50  0000 C CNN
F 2 "" H 5250 2300 50  0000 C CNN
F 3 "" H 5250 2300 50  0000 C CNN
	1    5250 2300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR044
U 1 1 5807807B
P 6150 2300
F 0 "#PWR044" H 6150 2150 50  0001 C CNN
F 1 "VCC" H 6150 2450 50  0000 C CNN
F 2 "" H 6150 2300 50  0000 C CNN
F 3 "" H 6150 2300 50  0000 C CNN
	1    6150 2300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR045
U 1 1 5807811F
P 7050 2300
F 0 "#PWR045" H 7050 2150 50  0001 C CNN
F 1 "VCC" H 7050 2450 50  0000 C CNN
F 2 "" H 7050 2300 50  0000 C CNN
F 3 "" H 7050 2300 50  0000 C CNN
	1    7050 2300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR046
U 1 1 580781C3
P 7950 2300
F 0 "#PWR046" H 7950 2150 50  0001 C CNN
F 1 "VCC" H 7950 2450 50  0000 C CNN
F 2 "" H 7950 2300 50  0000 C CNN
F 3 "" H 7950 2300 50  0000 C CNN
	1    7950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2050 1350 2050
Wire Wire Line
	1850 1950 1700 1950
Connection ~ 1700 1950
$EndSCHEMATC
